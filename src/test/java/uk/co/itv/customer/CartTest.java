package uk.co.itv.customer;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import uk.co.itv.model.Item;

import static org.junit.Assert.*;

public class CartTest {

    Basket cart;

    @Before
    public void setUp() throws Exception {
        cart = new Basket();
    }

    @After
    public void tearDown() throws Exception {
        cart.currentBasket.clear();
    }

    @Test
    public void addItemSize() {
        int size = cart.currentBasket.size();
        cart.currentBasket.add(new Item("A", 10));
        Assert.assertEquals(size + 1, cart.currentBasket.size());
    }

    @Test
    public void priceCorrectlyIncrements() {
        cart.currentBasket.add(new Item("A", 20));
        cart.currentBasket.add(new Item("A", 20));
        cart.currentBasket.add(new Item("A", 20));
        int expected = 60;
        assertEquals(expected, cart.getTotal());
    }

    @Test
    public void priceCorrectAfterRemoval() {
        cart.currentBasket.add(new Item("A", 20));
        cart.currentBasket.add(new Item("A", 20));
        cart.currentBasket.add(new Item("A", 20));
        Item i = cart.currentBasket.get(0);
        cart.removeItem(i);
        int expected = 40;
        System.out.println(cart.currentBasket);
        assertEquals(expected, cart.getTotal());
    }
}