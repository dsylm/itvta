package uk.co.itv.model;

/**
 * POJO class to hold the items that can be bought
 */
public class Item {

    /**
     * The ID of the SKU
     */
    public final String id;

    /**
     * The price of the SKU
     */
    public final int price;

    public Item(String id, int price) {
        this.id = id;
        this.price = price;
    }

    @Override
    public String toString() {
        return id;
    }
}
