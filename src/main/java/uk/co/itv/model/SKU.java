package uk.co.itv.model;

import java.util.HashMap;

/**
 * A container class to hold {@link Item}
 */
public class SKU {

    public HashMap<String, Item> product = new HashMap<>();

    public void addItem(String id, int price) {
        if(!product.containsKey(id)) {
            product.put(id, new Item(id, price));
        }
    }

    public Item getItem(String id) {
        return product.getOrDefault(id, null);
    }

    public void clearSKUS() {
        product.clear();
    }

}
