package uk.co.itv.store;

import java.util.ArrayList;

/**
 * A container class to hold the {@link Offer}
 */
public class SpecialOffer {

    /**
     * A collection of offers
     */
    public ArrayList<Offer> offers = new ArrayList<>();

    /**
     * Adding a new {@link Offer} to the collection
     * @param item the name of the offer
     * @param quant the quantity of the item required to reach the offer
     * @param price the price of the offer
     */
    public void addOffer(String item, int quant, int price) {
        offers.add(new Offer(item, quant, price));
    }

    /**
     * Returns a specific offer based on an item name
     * @param item the item name to search for
     * @return the {@link Offer} that matches the input
     */
    public Offer getOffer(String item) {
        for(Offer o : offers) {
            if(o.item.equals(item)) {
                return o;
            }
        }
        return null;
    }

    public void clearOffers() {
        offers.clear();
    }
}
