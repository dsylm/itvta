package uk.co.itv.store;

/**
 * A POJO to define a Offer that can be found within the store
 */
public class Offer {

    public final String item;
    public final int quant, price;

    public Offer(String item, int quant, int price) {
        this.item = item;
        this.quant = quant;
        this.price = price;
    }
}
