package uk.co.itv.customer;

import uk.co.itv.model.Item;
import uk.co.itv.store.Offer;

import java.util.ArrayList;

/**
 * A representation of a shopping cart which will contain a customers items
 */
public class Basket {

    ArrayList<Item> currentBasket = new ArrayList<>();

    /**
     * Adds a {@link Item} to the current currentBasket
     * @param item the item to add to the currentBasket
     */
    public void addItem(Item item) {
        currentBasket.add(item);
    }

    /**
     * Calculates the current total price of the currentBasket
     * @return the total price of items within the currentBasket
     */
    int getTotal() {
        int price = 0;
        for(Item i : currentBasket) {
            price = price + i.price;
        }
        return price;
    }

    /**
     * Removes a {@link Item} from the currentBasket
     * @param item the item to remove from the currentBasket
     */
    void removeItem(Item item) {
        currentBasket.remove(item);
    }

    /**
     * Offers a visual representation of what is included in the basket
     * alongside with the total price
     * @return the basket content with its price
     */
    public String printBasket() {
        StringBuilder sb = new StringBuilder();
        for (Item i : currentBasket) {
            sb.append(i.id);
        }
        sb.append("\n Total is: " + getTotal());
        return sb.toString();
    }

    /**
     * Manages the special offer interaction with the currentBasket when
     * the customer reaches the checkout
     * @param offer the special offer to check against the currentBasket
     */
    public void offer(Offer offer) {
        int q = 0;
        for (Item i: currentBasket) {
            if(i.id.equals(offer.item))  {
                q++;
            }
        }
        if(q >= offer.quant) {
            for(int i = 0; i < offer.quant; i++) {
                currentBasket.removeIf(item -> item.id.equals(offer.item));
            }
            addItem(new Item(offer.item, offer.price));
        }
    }

    public void clearBasket() {
        currentBasket.clear();
    }
}
