package uk.co.itv;


import uk.co.itv.customer.Basket;
import uk.co.itv.model.Item;
import uk.co.itv.model.SKU;
import uk.co.itv.store.Offer;
import uk.co.itv.store.SpecialOffer;

public class Worker {

    SKU sku;
    Basket cart;
    SpecialOffer so;


    private Worker() {
        sku = new SKU();
        cart = new Basket();
        so = new SpecialOffer();
        launch();
    }

    private void launch() {
        //Add items into the store with the key {"product name"}, {"price"}
        String[][] items = {{"A", "90"}, {"B", "50"}};
        addItems(items);

        //Add offers into the store with the key {"offer target(item id)"}, {"quantity req"}, {"new price"}
        String[][] offers = {{"A", "2", "75"}};
        addOffers(offers);

        //Add items into the customer basket with the key {Item}
        Item[] basketItems = { sku.getItem("A"), sku.getItem("B"), sku.getItem("A") };
        addToBasket(basketItems);


        System.out.println(cart.printBasket()); //print the basket pre-offer
        applyOffers(); //check and apply offers
        System.out.println(cart.printBasket()); //print the basket post-offer
    }

    private void addItems(String[][] item) {
        for (String[] anItem : item) {
            sku.addItem(anItem[0], Integer.valueOf(anItem[1]));
        }
    }

    private void addOffers(String[][] offers) {
        for (String[] anOffer : offers) {
            so.addOffer(anOffer[0], Integer.valueOf(anOffer[1]), Integer.valueOf(anOffer[2]));
        }
    }
    private void addToBasket(Item[] basketItems) {
        for(Item anItem : basketItems) {
            cart.addItem(anItem);
        }
    }

    private void applyOffers() {
        for(Offer o : so.offers) {
            cart.offer(o);
        }
    }

    public static void main(String[] args) {
        new Worker();
    }

}